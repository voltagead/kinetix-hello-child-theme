# Kinetix/Hello Elementor Child Theme #

This is a custom child-theme developed to work as a starter child theme that incorporates Voltage's Kinetix with Elementor's "Hello Elementor" theme and Elementor Pro capabilities for custom development (relies on Bootstrap for FE) and Elementor(Pro).


## Dependencies

This build leverages Node, NPM and Gulp which must be installed globally for installation to complete.

- Node - v14.x.x(at build time) - https://nodejs.org/en/download/package-manager/
- NPM  - v6.x.x(at build time) - http://blog.npmjs.org/post/85484771375/how-to-install-npm
- Yarn - https://yarnpkg.com/en/docs/install
    + Yarn Tasks/flags: https://yarnpkg.com/lang/en/docs/migrating-from-npm/
- Gulp CLI - https://www.npmjs.com/package/gulp-cli `yarn global add gulp-cli`/

## Wordpress Dependencies/Requirements
- Hello Elementor parent theme installed (https://elementor.com/hello-theme/)
- VOLTAGE Kinetix plugin activated (https://bitbucket.org/voltagead/kinetix-plugin/src/master/) 
- Elementor & Elementor Pro plugins activated (https://elementor.com/)

# Development #

- Follow VOLTAGE's development workflow found [here](https://sites.google.com/a/voltagead.com/operations-intranet/departments/development/general-workflow)


## Installation

1. `git clone git@bitbucket.org:voltagead/kinetix-hello-child-theme.git {{PROJECT_NAME}}` into your webroot (replace `{{PROJECT_NAME}}` with the directory/project root directory name)
2. `cd {{PROJECT_NAME}}`
3. Disconnect _this_ Git/repo, rename child-theme files, then commit to a new `{{project_NAME}}` Repo.
3. (Optional) Set up a vhost for `local.{{PROJECT_NAME}}.com` and point it to `"your_webroot"/{{PROJECT_NAME}}`. 
4. Navigate to the kinetix directory: `cd wp-content/themes/{{PROJECT_NAME}}/kinetix` then `yarn` to install Node dependencies
5. Start gulp watch `gulp watch` (from inside the `kinetix` directory and begin development
6. Gulp commands include:
    `gulp` - default - build 'local development' files (non-minified)
    `gulp watch` -  "Watches" scss/js files and auto-refreshes local environment (use LiveReload chrome extension - turn on in browser while 'watching')
    `gulp stage` - build compressed/minified files for staging/production
    `gulp prod` - (not working currently) - same as 'gulp stage' but is also supposed to run some image minification/compressions on images inside 'kinetix' directory - this part isn't working so just use `gulp stage`

## Deployment
Follow VOLTAGE rules/deployment standards for branching/pull requests, etc then:

1. setup git your local remotes for WPEngine environments [here](https://my.wpengine.com/installs/barnesbullets/git_push)
2. When ready to deploy:
    a. first run `gulp stage` add/commit those files to the RC/Preview branch
    b. then `git push origin {BRANCH_NAME}` so your code/branch is in Bitbucket
    c. then `git push {WPENGINE_REMOTE_NAME} {BRANCH_NAME}`

