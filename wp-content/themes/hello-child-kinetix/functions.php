<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */
function hello_elementor_child_enqueue_scripts() {
	wp_enqueue_style(
		'hello-elementor-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		[
			'hello-elementor-theme-style',
		],
		'1.0.0'
	);
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts', 20 );


// VOLTAGE CUSTOM
add_filter('use_block_editor_for_post','__return_false');

// require_once( get_stylesheet_directory() . '/inc/class-primary-nav-walker.php' );

// require_once( get_stylesheet_directory() . '/inc/class-primary-nav-walker-mobile.php' );

// require_once( get_stylesheet_directory() . '/inc/custom-elementor-locations.php' );

// require_once( get_stylesheet_directory() . '/inc/woocommerce.php' );

function load_elementor_widgets() {
	require_once( get_stylesheet_directory() . '/inc/class-elementor-product-slider.php' );
	\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor_Product_Slider() );
}
// add_action( 'init', 'load_elementor_widgets' );