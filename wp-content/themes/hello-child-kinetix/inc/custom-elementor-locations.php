<?php
function __project_register_elementor_locations( $elementor_theme_manager ) {
	$elementor_theme_manager->register_all_core_location();
	$elementor_theme_manager->register_location(
		'single-bottom',
		array(
			'label' => __( 'Single Bottom', '__project' ),
			'multiple' => true,
			'edit_in_content' => true
		)
	);
}
add_action( 'elementor/theme/register_locations', '__project_register_elementor_locations' );
