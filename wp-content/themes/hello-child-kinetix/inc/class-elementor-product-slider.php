<?php
class Elementor_Product_Slider extends \Elementor\Widget_Base {

	public function __construct($data = [], $args = null) {
		parent::__construct( $data, $args );
	}

	public function get_name() {
		return 'product-slider';
	}

	public function get_title() {
		return 'Product Slider';
	}

	public function get_icon() {
		return 'eicon-slider-push';
	}

	public function get_categories() {
		return array( 'general' );
	}

	protected function _register_controls() {
		$products = wc_get_products( array( 'posts_per_page' => -1 ) );
		$options = array();

		foreach ( $products as $product ) {
			$options[$product->get_id()] = $product->get_name();
		}

		$this->start_controls_section(
			'content_section',
			[
				'label' => 'Settings',
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'products',
			[
				'label' => 'Products',
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $options
			]
		);

		$this->add_control(
			'layout',
			[
				'label' => 'Layout',
				'type' => \Elementor\Controls_Manager::SELECT,
				'multiple' => true,
				'default' => 'dt-4-slide',
				'options' => array( 'dt-4-slide' => 'Desktop 4 Slide', 'dt-1-slide' => 'Desktop 1 Slide' )
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();
		$products = $settings['products'];
		$layout = $settings['layout'];
		?>
			<div class="pdp-upsells">
				<section class="up-sells upsells">
					<div class="js-product-slider pdp-upsells__slider" data-layout="<?php echo $layout; ?>">
						<?php foreach ( $products as $product ) : ?>
							<?php $product = wc_get_product( $product ); ?>
							<div class="pdp-upsells__slider-slide text-center">
								<a class="pdp-upsells__img-link" href="<?php echo $product->get_permalink(); ?>">
									<?php echo $product->get_image( 'woocommerce_single' ); ?>
								</a>
								<a class="pdp-upsells__slide-content d-block" href="<?php echo $product->get_permalink(); ?>">
									<h2 class="pdp-upsells__title"><?php echo $product->get_title(); ?></h2>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</section>
			</div>
		<?php
	}
}
