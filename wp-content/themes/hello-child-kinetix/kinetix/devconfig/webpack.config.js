var path = require( 'path' ),
	webpack = require( 'webpack' ),
	VueLoaderPlugin = require( 'vue-loader/lib/plugin' );

const ENV = process.env.NODE_ENV;
const IS_PROD = process.env.NODE_ENV === 'dist';

var config = {
	mode: ( IS_PROD ) ? 'production' : 'development',
	entry: {
		site: 'site'
	},
	output: {
		path: path.join( __dirname, '/../assets/' + ENV ),
		publicPath: ( IS_PROD ) ? '/wp-content/themes/hello-child-kinetix/kinetix/assets/dist/' : '/wp-content/themes/hello-child-kinetix/kinetix/assets/build/',
		filename: ( IS_PROD ) ? '[name].min.js' : '[name].js',
		chunkFilename: ( IS_PROD ) ? '[name].[hash].min.js' : '[name].[hash].js',
		sourceMapFilename:  path.join( '../maps/[name].js.map' )
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			},
			{
				test: /\.js$/,
				enforce: 'pre',
				include: [
					path.join( __dirname, '/../assets/js' )
				],
				exclude: [
					/node_modules/,
					path.join( __dirname, '/../assets/js/nolint' )
				],
				use: [
					{
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env']
						}
					},
					{
						loader: 'eslint-loader',
						options: {
							failOnWarning: false,
							failOnError: true,
							configFile: path.join( __dirname, '/eslintrc.json' ),
							fix: true // Set this to false if the auto fix options is causing problems
						}
					}
				]
			},
			{
				test: /\.css$/,
				oneOf: [
					// this matches plain `<style>` or `<style scoped>`
					{
						use: [
							'vue-style-loader',
							'css-loader',
							'sass-loader'
						]
					}
				]
			}
		]
	},
	externals: {
		jquery: 'jQuery'
	},
	plugins: [
		new VueLoaderPlugin(),
		new webpack.HashedModuleIdsPlugin(),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery'
		})
	],
	resolve: {
		modules: [
			path.join( __dirname, '/../node_modules' ),
			path.join( __dirname, '/../assets/js' ),
			path.join( __dirname, '/../assets/js/components' ),
			path.join( __dirname, '/../components' )
		],
		extensions: [ '.js', '.vue', '.json' ],
		alias: {
			'vue$': 'vue/dist/vue.esm.js'
		},
	},
	stats: {
		assets: true,
		cached: false,
		cachedAssets: false,
		children: false,
		colors: true,
		errors: true,
		errorDetails: false,
		hash: false,
		modules: false,
		warnings: true,
		version: false
	},
	target: 'web',
	watchOptions: {
		ignored: [ path.join( __dirname, '/../node_modules/' ), path.join( __dirname, '/../assets/lib/' ) ]
	},
	devtool: ( IS_PROD ) ? 'none' : 'eval-source-map',
	cache: ( IS_PROD ) ? false : true
};
module.exports = config;

