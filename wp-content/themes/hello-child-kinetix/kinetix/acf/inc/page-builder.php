<?php
acf_add_local_field_group( array(
	'key' => 'kinetix_page_builder_group',
	'title' => 'Kinetix',
	'fields' => array(
		array(
			'key' => 'kinetix_page_builder',
			'label' => 'Page Builder',
			'name' => 'kinetix_page_builder',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'button_label' => 'Add Component',
			'min' => '',
			'max' => '',
			'layouts' => array(),
		)
	),
	'location' => array(
		array(
			array(
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'kinetix/template-kinetix-page-builder.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array(
		0 => 'the_content',
		1 => 'excerpt',
		2 => 'discussion',
		3 => 'comments',
		4 => 'format',
		5 => 'featured_image',
		6 => 'send-trackbacks',
	),
	'active' => 1,
	'description' => '',
) );

?>
