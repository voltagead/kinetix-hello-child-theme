<?php
// if( function_exists('acf_add_options_page') ) {

// 	acf_add_options_page( array(
// 		'page_title' 	=> 'Theme General Settings',
// 		'menu_title'	=> 'Theme Settings',
// 		'menu_slug' 	=> 'theme-general-settings',
// 		'capability'	=> 'edit_posts',
// 		'redirect'		=> false
// 	) );

// 	acf_add_local_field_group(array(
// 		'key' => 'group_5f534cea0506p',
// 		'title' => 'Header',
// 		'fields' => array(
// 			array(
// 				'key' => 'field_5f4d9r8s1913c',
// 				'label' => 'Announcement Banner',
// 				'name' => 'announcement_banner',
// 				'type' => 'text',
// 				'instructions' => '',
// 				'required' => 0,
// 				'conditional_logic' => 0,
// 				'wrapper' => array(
// 					'width' => '',
// 					'class' => '',
// 					'id' => '',
// 				),
// 				'default_value' => 'Announcement Banner',
// 				'placeholder' => '',
// 				'prepend' => '',
// 				'append' => '',
// 				'maxlength' => '',
// 			),
// 		),
// 		'location' => array(
// 			array(
// 				array(
// 					'param' => 'options_page',
// 					'operator' => '==',
// 					'value' => 'theme-general-settings',
// 				),
// 			),
// 		),
// 		'menu_order' => 0,
// 		'position' => 'normal',
// 		'style' => 'default',
// 		'label_placement' => 'top',
// 		'instruction_placement' => 'label',
// 		'hide_on_screen' => '',
// 		'active' => true,
// 		'description' => '',
// 	));
// }
