// console.log('kinetix/assets/js/header.js');
// Use if needed or customize

// $( document ).ready( function() {

// 	// Hamburger
// 	let $hamburger = $( '.js-mobile-menu-trigger' );
// 	$hamburger.on( 'click', function() {
// 		$hamburger.toggleClass( 'is-active' );
// 		$hamburger.attr( 'aria-expanded',
// 		   $hamburger.attr( 'aria-expanded' ) == 'false' ? 'true' : 'false'
// 		);
// 		$( '.mobile-menu' ).css( 'top', $( '.header' ).outerHeight( true ) );
// 		$( 'body' ).toggleClass( 'mobile-menu-active' );
// 	} );

// 	// Header menu media items
// 	$( '[id^="js-menu-media-"]' ).each( function() {
// 		let $mediaMenuItem = $( this ),
// 			json = JSON.parse( $mediaMenuItem.html() ),
// 			html;
// 		html = '<li class="header__desktop-nav-panel-media">' + json.heading + json.subheading + json.image + json.cta + '</li>';
// 		$mediaMenuItem.siblings( '.js-desktop-nav-panel' ).find( '.header__desktop-nav-panel-scroll' ).children( 'ul' ).append( html );
// 	} );
// } );

// // Mobile Menu
// $( '.mobile-menu .menu > li > ul' ).on( 'show.bs.collapse', function() {
// 	$( this ).parent().find( '.hamburger' ).addClass( 'is-active' );
// } );

// $( '.mobile-menu .menu > li > ul' ).on( 'hide.bs.collapse', function() {
// 	$( this ).parent().find( '.js-mobile-menu-collapse-btn' ).removeClass( 'is-active' );
// } );

// $( '.js-mobile-menu-collapse-btn' ).on( 'click', function() {
// 	$( this ).closest( '.menu-item' ).children( '.collapse' ).collapse( 'toggle' );
// } );

// // Desktop menu
// $( '.js-desktop-nav-panel' ).on( 'show.bs.collapse', function() {
// 	$( this ).css( 'top', $( '.header .header__desktop' ).outerHeight() );
// 	$( 'body' ).addClass( 'desktop-menu-open' );
// } );

// $( '.js-desktop-nav-panel' ).on( 'hide.bs.collapse', function() {
// 	$( 'body' ).removeClass( 'desktop-menu-open' );
// } );

// $( '#js-header-desktop-navigation .menu > li' ).hover(
// 	function() {
// 		if ( $.isFunction( $.fn.collapse ) ) {
// 			$( this ).children( '.js-desktop-nav-panel' ).collapse( 'show' );
// 		}
// 	}
// );

// $( '.header__desktop' ).mouseleave( function() {
// 	if ( $.isFunction( $.fn.collapse ) ) {
// 		$( '.js-desktop-nav-panel' ).collapse( 'hide' );
// 	}
// } );
